var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
var prb = (i, times) => {
    books.length > 0 ? readBooksPromise(times, books[i])
    .then(fulfilled => {return prb(i+1, fulfilled)})
    .catch(error => {}) : ''
}
prb(0, 10000)