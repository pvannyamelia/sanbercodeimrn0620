import React, { Component } from 'react'
import { View, StyleSheet, Text, Image, ScrollView } from 'react-native'

export default class App extends Component {
  render() {
    return (
    //   <View style={styles.container}>
    //       <Image
    //     style={styles.image}
    //     source={{uri: 'http://www.reactnativeexpress.com/static/logo.png'}}
    //   />
    //     <View style={styles.box} />
    //     <Text style={styles.text}>Hello!</Text>
    //   </View>
    
    <ScrollView style={styles1.container}>
        <View style={styles1.boxLarge} />
        <ScrollView horizontal>
          <View style={styles1.boxSmall} />
          <View style={styles1.boxSmall} />
          <View style={styles1.boxSmall} />
        </ScrollView>
        <View style={styles1.boxLarge} />
        <View style={styles1.boxSmall} />
        <View style={styles1.boxLarge} />
      </ScrollView>
    )
  }
}

const styles1 = StyleSheet.create({
    container: {
      flex: 1,
    },
    boxSmall: {
      width: 200,
      height: 200,
      marginBottom: 10,
      marginRight: 10,
      backgroundColor: 'skyblue',
    },
    boxLarge: {
      width: 300,
      height: 300,
      marginBottom: 10,
      marginRight: 10,
      backgroundColor: 'steelblue',
    },
  })

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  box: {
    width: 200,
    height: 200,
    backgroundColor: 'skyblue',
    borderWidth: 2,
    borderColor: 'steelblue',
    borderRadius: 20,
  },
  text: {
    backgroundColor: 'whitesmoke',
    color: '#4A90E2',
    fontSize: 24,
    padding: 10,
  },
  image: {
    width: 200,
    height: 200,
  },
})