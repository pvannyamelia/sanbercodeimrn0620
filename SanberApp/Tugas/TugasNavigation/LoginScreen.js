import React, {Component} from 'react';
import {StyleSheet, Image, TextInput, TouchableOpacity, View, Text, ScrollView} from 'react-native';

export default class App extends Component{
    render(){
        return(
            <ScrollView style={styles.container}>
            <View style={styles.containerLogo}>
                <Image
                source={require('./assets/logo.png')}
                style={styles.logo}/>
            </View>
            <View style={styles.containerForm}>
                <Text style={styles.h1}>Login</Text>
                <Text style={styles.label}>Username/Email</Text>
                <TextInput style={styles.input}></TextInput>
                <Text style={styles.label}>Password</Text>
                <TextInput style={styles.input}></TextInput>
            </View>
            <View style={styles.containerButton}>
                <TouchableOpacity style={styles.btLogin} onPress={() => this.props.navigation.toggleDrawer()}>
                    <Text style={styles.lblButton}>Masuk</Text>
                    <Text style={styles.lblButton, {fontSize: 10, color: 'white'}}>Menuju Skill Screen</Text>
                </TouchableOpacity>
                <Text style={styles.h2}>atau</Text>
                <TouchableOpacity style={styles.btDaftar} onPress={() => this.props.navigation.push('Register')}>
                    <Text style={styles.lblButton}>Daftar?</Text>
                </TouchableOpacity>
            </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
container: {
    backgroundColor: "#fff"
    },
containerLogo:{
    flex: 1,
    marginTop: 0
},

containerForm:{
    flex: 2,
    marginTop: 20
},
h1:{
    color: '#003366',
    fontSize: 25,
    textAlign: 'center',
},
label:{
    marginTop: 20,
    marginLeft: 40,
    marginBottom: 5
},
input:{
    borderColor: '#003366',
    height: 50,
    borderWidth:1,
    marginHorizontal: 40
},

containerButton:{
    flex: 1,
    marginVertical: 50,
    alignItems: 'center'
},
btLogin:{
    backgroundColor: '#3EC6FF',
    height: 50,
    width: 150,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15,
},
lblButton:{
    fontSize: 20,
    color: 'white'
},
h2:{
    marginVertical: 10,
    fontSize:20,
    textAlign:'center',
    color:'#3EC6FF'
},
btDaftar:{
    backgroundColor: '#003366',
    height: 40,
    width: 150,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15,
},
})