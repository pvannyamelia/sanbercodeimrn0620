import React, {Component} from 'react';
import {StyleSheet, Image, View, Text, ScrollView, TextComponent, TouchableOpacity} from 'react-native';
import { FontAwesome, AntDesign } from '@expo/vector-icons'; //https://icons.expo.fyi/

export default class App extends Component{
    render(){
        return(
            <ScrollView style={styles.container}>
                <TouchableOpacity style={{flexDirection: 'row', alignSelf: "flex-start", margin: 20}} onPress={() => this.props.navigation.goBack()}><AntDesign name="arrowleft" size={30} color="#003366" /></TouchableOpacity>
                <Text style={styles.h1}>Tentang Saya</Text>
                <FontAwesome name="user-circle-o" size={150} color="#CDCDCD" style={styles.displaypic} />
                <Text style={styles.h2}>John Doe</Text>
                <Text style={styles.h3}>React Native Developer</Text>
                <View style={styles.content}>
                    <Text style={styles.prgf}>Portofolio</Text>
                    <View style={styles.row}>
                        <View style={styles.column}>
                            <FontAwesome name="gitlab" size={35} color="#3EC6FF" />
                            <Text style={styles.prgfB}>@_johndoe</Text>
                        </View>
                        <View style={styles.column}>
                            <AntDesign name="github" size={35} color="#3EC6FF" />
                            <Text style={styles.prgfB}>@john.doe</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.content}>
                    <Text style={styles.prgf}>Hubungi Saya</Text>
                    <View style={{alignItems: 'flex-start', marginLeft: 80}}>
                        <View style={styles.row}>
                            <AntDesign name="facebook-square" size={35} color="#3EC6FF" style={{marginHorizontal:15}}/>
                            <Text style={styles.prgfB}>johnn.doe</Text>
                        </View>
                        <View style={styles.row}>
                            <AntDesign name="instagram" size={35} color="#3EC6FF" style={{marginHorizontal:15}}/>
                            <Text style={styles.prgfB}>@john.doe123</Text>
                        </View>
                        <View style={styles.row}>
                            <AntDesign name="twitter" size={35} color="#3EC6FF" style={{marginHorizontal:15}}/>
                            <Text style={styles.prgfB}>@johnn.doe</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff"
      },
    h1:{
        marginTop: 0,
        color: '#003366',
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    h2:{
        color: '#003366',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    h3:{
        color: '#3EC6FF',
        fontSize: 15,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    displaypic:{
        alignSelf: 'center',
        marginVertical: 20,
    },
    content:{
        marginVertical: 10,
        backgroundColor: '#EFEFEF',
        marginHorizontal: 8,
        borderRadius: 10
    },
    prgf:{
        marginLeft: 10,
        color: '#003366',
        paddingVertical:2,
        borderBottomColor: '#003366',
        borderBottomWidth: 1
    },
    prgfB:{
        color: '#003366',
        paddingVertical:2,
        fontWeight: 'bold',
    },
    row:{
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
    },
    column:{
        marginHorizontal: 15,
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
    }
})