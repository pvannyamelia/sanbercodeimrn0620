import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import data from './../skillData.json';
//import  from '';
export default class Skill extends React.Component {
    render(){
        let skill = this.props.skill;
        return(
            <View style={styles.container}>
                <MaterialCommunityIcons name={skill.iconName} size={80} color="#003366" style={{marginRight: 30, marginLeft: 5, marginVertical: 20}}/>
                <View style={{flexDirection:'column', width: 150, marginRight: 20}}>
                    <Text style={styles.title}>{skill.skillName}</Text>
                    <Text style={styles.subtitle}>{skill.categoryName}</Text>
                    <Text style={styles.percent}>{skill.percentageProgress}</Text>
                </View>
                <TouchableOpacity style={{flexDirection: 'column', alignSelf: 'center'}}>
                    <MaterialCommunityIcons name="chevron-right" size={60} color="#003366" />
                </TouchableOpacity>
            </View>
        )
}
}

const styles= StyleSheet.create({
    container:{
        backgroundColor: '#B4E9FF',
        borderRadius: 10,
        flexDirection: 'row',
        elevation: 3,
        margin: 5
    },
    title: {
        fontWeight: 'bold', 
        color: '#003366', 
        fontSize: 20,
        marginTop: 5
    },
    subtitle:{
        color: '#2DB5EE',
        fontWeight: 'bold',
        fontSize: 16
    },
    percent:{
        alignSelf: 'flex-end',
        fontWeight: 'bold',
        fontSize: 45,
        color: 'white'
    }
})