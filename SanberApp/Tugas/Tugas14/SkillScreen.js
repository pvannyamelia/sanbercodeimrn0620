import React from 'react';
import {StyleSheet, View, Text, ScrollView, Image, FlatList} from 'react-native';
import { FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons'; 
import data from './skillData.json';
import SkillItem from './components/Skills';
//import  from '';
export default class Skill extends React.Component {
    render(){
        return(
            <View>
                <Image 
                source={require('./../Tugas13/assets/logo.png')}
                style={styles.logo}/>
                <View style={styles.identity}>
                    <FontAwesome name="user-circle" size={30} color="#3EC6FF" style={{marginHorizontal: 10}} />
                    <View>
                        <Text style={{fontSize: 12}}>Hai,</Text>
                        <Text style={{color: '#003366', fontSize: 15}}>John Doe</Text>
                    </View>
                </View>
                <Text style={{fontSize: 20, color: '#003366', borderBottomColor: '#3EC6FF', borderBottomWidth: 2, margin: 10, padding: 2}}>SKILL</Text>
                <View style={styles.row}>
                    <View style={styles.skillContainer}>
                        <Text style={styles.skill}>Library/Framework</Text>
                    </View>
                    <View style={styles.skillContainer}>
                        <Text style={styles.skill}>Bahasa Pemrograman</Text>
                    </View>
                    <View style={styles.skillContainer}>
                        <Text style={styles.skill}>Teknologi</Text>
                    </View>
                </View>
                <FlatList
                    data={data.items}
                    renderItem={(skill)=><SkillItem skill={skill.item} />
                    }
                    keyExtractor={(item)=>item.id}
                    style={{marginTop:5, marginBottom: 260}}
                    />
            </View>
        )
}
}

const styles= StyleSheet.create({
    logo:{
        resizeMode: 'contain',
        width: 200,
        flexDirection: "row",
        alignSelf: "flex-end",
    },
    identity:{
        flexDirection: 'row',
        marginLeft: 10
    },
    row:{
        flexDirection: 'row',
        alignContent: 'center',
        marginHorizontal: 10,
        marginTop: 0
    },
    skillContainer:{
        backgroundColor: '#B4E9FF',
        borderRadius: 10,
        padding: 8,
        marginRight: 4
    },
    skill:{
        fontSize: 12,
        color: '#003366',
        fontWeight: 'bold'
    }
})