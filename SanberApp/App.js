import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import Tugas from './Tugas/Tugas15/index';
import TugasNavigation from './Tugas/TugasNavigation/index';
import Quiz from './Tugas/Quiz3/index';
import Latihan from './Latihan/LifeCycle/createComponent';

export default function App() {
  return (
    // <Tugas />
    // <TugasNavigation />
    <Quiz />
    // <Latihan />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
